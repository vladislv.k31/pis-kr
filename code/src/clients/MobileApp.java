package clients;

import microservices.Auth;
import microservices.Blog;

import java.util.Arrays;

public class MobileApp {

    public MobileApp() {
        Blog blog = Blog.getInstance();
        System.out.println(Arrays.toString(blog.getArticles()));

        Auth auth = Auth.getInstance();
        auth.login();
        auth.register();

        System.out.println("Blog ms ID: " + System.identityHashCode(blog));
        System.out.println("Auth ms ID: " + System.identityHashCode(auth));
    }

}
