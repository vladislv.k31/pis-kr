package microservices;

import java.lang.reflect.Array;

public class Blog {

    private static Blog instance;

    private Blog() {}

    public static Blog getInstance() {
        if (instance == null) instance = new Blog();
        return instance;
    }

    public String[] getArticles() {
        String[] articles = new String[3];
        articles[0] = "First";
        articles[1] = "Second";
        articles[2] = "Third";

        return articles;
    }

}
