package microservices;

public class Auth {

    private static Auth instance;

    private Auth() {}

    public static Auth getInstance() {
        if (instance == null) instance = new Auth();
        return instance;
    }

    public void login() {
        System.out.println("Method for login");
    }

    public void register() {
        System.out.println("Method for register");
    }

}
